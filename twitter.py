import tweepy, keys

location_id = 23424977

def get_api():
    auth = tweepy.OAuthHandler(keys.CONSUMER_KEY, keys.CONSUMER_SECRET)
    auth.set_access_token(keys.ACCESS_KEY, keys.ACCESS_SECRET)
    return tweepy.API(auth)

def get_trends(api):
    trends_raw = api.trends_place(location_id)
    trends = trends_raw[0]['trends']
    return trends
