import random, tweepy, time, sys, twitter

#open file with tweet templates
argfile = 'tweets/tweets.txt'
filename=open(argfile,'r')
tweet_templates=filename.readlines()
filename.close()

#get tweepy api
api = twitter.get_api();

#get top 10 trends
trends = twitter.get_trends(api)

while (1):
    #pick a tweet templae
    tweet = random.choice(tweet_templates)
    #pick a hastag from the trends
    hashtag = random.choice(trends)['name']
    #add the hashtag to the tweet
    tweet = tweet.replace("#tag", hashtag)
    
    #tweet that shit. catch errors
    try:
        api.update_status(tweet)
    except Exception, e:
        pass
    
    #wait a random amount of time before the next tweet
    time.sleep(random.randint(300,900))


